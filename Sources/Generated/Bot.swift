import SwiftyJSON

public struct Bot {
    public let id: String?
    public let botid: Double?
    public let name: String?

    public init(id: String?, botid: Double?, name: String?) {
        self.id = id
        self.botid = botid
        self.name = name
    }

    public init(json: JSON) throws {
        // Required properties

        // Optional properties
        if json["id"].exists() &&
           json["id"].type != .string {
            throw ModelError.propertyTypeMismatch(name: "id", type: "string", value: json["id"].description, valueType: String(describing: json["id"].type))
        }
        self.id = json["id"].string
        if json["botid"].exists() &&
           json["botid"].type != .number {
            throw ModelError.propertyTypeMismatch(name: "botid", type: "number", value: json["botid"].description, valueType: String(describing: json["botid"].type))
        }
        self.botid = json["botid"].number.map { Double($0) }
        if json["name"].exists() &&
           json["name"].type != .string {
            throw ModelError.propertyTypeMismatch(name: "name", type: "string", value: json["name"].description, valueType: String(describing: json["name"].type))
        }
        self.name = json["name"].string

        // Check for extraneous properties
        if let jsonProperties = json.dictionary?.keys {
            let properties: [String] = ["id", "botid", "name"]
            for jsonPropertyName in jsonProperties {
                if !properties.contains(where: { $0 == jsonPropertyName }) {
                    throw ModelError.extraneousProperty(name: jsonPropertyName)
                }
            }
        }
    }

    public func settingID(_ newId: String?) -> Bot {
      return Bot(id: newId, botid: botid, name: name)
    }

    public func updatingWith(json: JSON) throws -> Bot {
        if json["id"].exists() &&
           json["id"].type != .string {
            throw ModelError.propertyTypeMismatch(name: "id", type: "string", value: json["id"].description, valueType: String(describing: json["id"].type))
        }
        let id = json["id"].string ?? self.id

        if json["botid"].exists() &&
           json["botid"].type != .number {
            throw ModelError.propertyTypeMismatch(name: "botid", type: "number", value: json["botid"].description, valueType: String(describing: json["botid"].type))
        }
        let botid = json["botid"].number.map { Double($0) } ?? self.botid

        if json["name"].exists() &&
           json["name"].type != .string {
            throw ModelError.propertyTypeMismatch(name: "name", type: "string", value: json["name"].description, valueType: String(describing: json["name"].type))
        }
        let name = json["name"].string ?? self.name

        return Bot(id: id, botid: botid, name: name)
    }

    public func toJSON() -> JSON {
        var result = JSON([
        ])
        if let id = id {
            result["id"] = JSON(id)
        }
        if let botid = botid {
            result["botid"] = JSON(botid)
        }
        if let name = name {
            result["name"] = JSON(name)
        }

        return result
    }
}
