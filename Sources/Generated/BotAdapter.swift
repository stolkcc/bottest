public protocol BotAdapter {
    func findAll(onCompletion: @escaping ([Bot], Error?) -> Void)
    func create(_ model: Bot, onCompletion: @escaping (Bot?, Error?) -> Void)
    func deleteAll(onCompletion: @escaping (Error?) -> Void)

    func findOne(_ maybeID: String?, onCompletion: @escaping (Bot?, Error?) -> Void)
    func update(_ maybeID: String?, with model: Bot, onCompletion: @escaping (Bot?, Error?) -> Void)
    func delete(_ maybeID: String?, onCompletion: @escaping (Bot?, Error?) -> Void)
}
